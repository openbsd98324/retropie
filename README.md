# retropie


## Stable and tested




RPI2 RPI3: 

573e75874b4b04c2138833f7ab29f007  cmretropie-4.5.1-rpi2_rpi3.img.gz 
(has sshd enabled.)

retropie-4.5.1-rpi2_rpi3.img.gz 
https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz

````
  wget -c --no-check-certificate   "https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz" 
````



## RPI0 

   wget -c --no-check-certificate   "https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi1_zero.img.gz"  


# Current (untested)

md5 224e64d8820fc64046ba3850f481c87e  https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi2_3_zero2w.img.gz




## More Links

Raspberry Pi 0, 1: 

    https://github.com/RetroPie/RetroPie-Setup/releases/download/3.7/retropie-v3.7-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/3.8.1/retropie-v3.8.1-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.0.2/retropie-4.0.2-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.1/retropie-4.1-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.2/retropie-4.2-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.3/retropie-4.3-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.4/retropie-4.4-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5/retropie-4.5-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.6/retropie-buster-4.6-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7/retropie-buster-4.7-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7.1/retropie-buster-4.7.1-rpi1_zero.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi1_zero.img.gz


Raspberry Pi 2B, 3B, 3B+,3A+, Zero 2W: 

    https://github.com/RetroPie/RetroPie-Setup/releases/download/3.7/retropie-v3.7-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/3.8.1/retropie-v3.8.1-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.0.2/retropie-4.0.2-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.1/retropie-4.1-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.2/retropie-4.2-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.3/retropie-4.3-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.4/retropie-4.4-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5/retropie-4.5-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.6/retropie-buster-4.6-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7/retropie-buster-4.7-rpi2_rpi3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7.1/retropie-buster-4.7.1-rpi2_3.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi2_3_zero2w.img.gz


Raspberry Pi 4B, 400: 

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.6/retropie-buster-4.6-rpi4.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7/retropie-buster-4.7-rpi4.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7.1/retropie-buster-4.7.1-rpi4_400.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi4_400.img.gz


Raspberry Pi Zero 2W:

    https://files.retropie.org.uk/images/weekly/

    https://files.retropie.org.uk/images/weekly/retropie-buster-4.7.19-rpi2_3.img.gz

    https://files.retropie.org.uk/images/weekly/retropie-buster-4.7.20-rpi2_3_zero2w.img.gz

    https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi2_3_zero2w.img.gz

Descarga las últimas versiones compiladas semanalmente(Para Raspberry Pi Zero 2W usar la misma que para Raspberry Pi 2-3):

    https://files.retropie.org.uk/images/weekly/

Manual install on top of existing OS:

    https://retropie.org.uk/docs/Manual-Installation/



# Customization 

![](medias/OpenPandora-machine-retropie.jpg)




